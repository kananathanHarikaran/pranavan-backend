
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Reservation = sequelize.define('Reservation', {
    date: DataTypes.DATE,
    reference_number: DataTypes.STRING,
    paid_amount: DataTypes.DECIMAL(10, 2),
    status: DataTypes.ENUM('Pending', 'Reserved', 'Completed', 'Cancelled', 'PartiallyPaid'),
    hotel_id: DataTypes.INTEGER,
    customer_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    
    is_active: DataTypes.BOOLEAN,
  }, {
    sequelize,
    modelName: 'Reservation',
    timestamps: true,
    underscored: true,
    paranoid: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  })
  Reservation.associate = function (models) {
    // associations can be defined here
  };
  
  return Reservation;
};