
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Room = sequelize.define('Room', {
    reference_number: DataTypes.STRING,
    name: DataTypes.STRING,
    phone: DataTypes.STRING,
    is_smoking: DataTypes.BOOLEAN,
    image: DataTypes.STRING,
    remarks: DataTypes.STRING,
    floor_id: DataTypes.INTEGER,
    room_type_id: DataTypes.INTEGER,
    price: DataTypes.DECIMAL(10,2),
    
    is_active: DataTypes.BOOLEAN,
  }, {
    sequelize,
    modelName: 'Room',
    timestamps: true,
    underscored: true,
    paranoid: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  })
  Room.associate = function (models) {
    // associations can be defined here
  };
  
  return Room;
};