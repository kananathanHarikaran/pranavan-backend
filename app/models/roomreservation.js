'use strict';
module.exports = (sequelize, DataTypes) => {
  const roomReservation = sequelize.define('roomReservation', {
    check_in: DataTypes.DATE,
    check_out: DataTypes.DATE,
    adults: DataTypes.INTEGER,
    child: DataTypes.INTEGER,
    room_id: DataTypes.INTEGER,
    reservation_id: DataTypes.INTEGER,
  
  }, {
    sequelize,
    modelName: 'roomReservation',
    timestamps: true,
    underscored: true,
    paranoid: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  })
  roomReservation.associate = function (models) {
    // associations can be defined here
  };
  
  return roomReservation;
};