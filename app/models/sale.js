'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sale = sequelize.define('Sale', {
    date: DataTypes.DATE,
    reference_number: DataTypes.STRING,
    paid_amount: DataTypes.DECIMAL(10,2),
    customer_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Sale',
    timestamps: true,
    underscored: true,
    paranoid: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  })
  Sale.associate = function (models) {
    // associations can be defined here
  };
  
  return Sale;
};