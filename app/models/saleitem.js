
'use strict';
module.exports = (sequelize, DataTypes) => {
  const SaleItem = sequelize.define('SaleItem', {
    quantity: DataTypes.INTEGER,
    amount: DataTypes.DECIMAL(10,2),
    items_id: DataTypes.INTEGER,
    sale_id: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'SaleItem',
    timestamps: true,
    underscored: true,
    paranoid: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  })
  SaleItem.associate = function (models) {
    // associations can be defined here
  };
  
  return SaleItem;
};