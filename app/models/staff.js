

'use strict';
module.exports = (sequelize, DataTypes) => {
  const staff = sequelize.define('staff', {
    name: DataTypes.STRING,
    
    is_active: DataTypes.BOOLEAN,
  }, {
    sequelize,
    modelName: 'staff',
    timestamps: true,
    underscored: true,
    paranoid: true,

    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  })
  staff.associate = function (models) {
    // associations can be defined here
  };
  
  return staff;
};