const express = require('express');
const router = express.Router();

/**      Auth route         */
router.use('/auth', require('./routers/auth/auth.router'));
/**      End auth route    */

/**      Customer route         */
router.use('/customers', require('./routers/customer/customer.router'));
/**      End customers route    */

/**      Settings route         */
router.use('/users', require('./routers/setting/user.router'));
router.use('/hotels', require('./routers/setting/hotel.router'));
/**      End settings route    */

/**      Master Data route         */
router.use('/designations', require('./routers/masterData/designation.router'));
router.use('/floors', require('./routers/masterData/floor.validator'));
router.use('/room-types', require('./routers/masterData/roomtype.router'));
/**      End Master Data route    */

/**      Bar route         */
router.use('/items', require('./routers/bar/item.router'));
/**      End Bar route    */

module.exports = router;
